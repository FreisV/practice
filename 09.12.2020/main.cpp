#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <fstream>

#define foreach(i, arr) for (auto i : arr)

using namespace std;

void taskOne()
{
  vector<int> arr;
  vector<int> res;
  size_t size;

  cin >> size;
  arr.resize(size);

  for (size_t i = 0; i < size; i++)
    cin >> arr[i];

  for (size_t i = 0; i < arr.size(); i++)
  {
    if (arr[i] % 2 == 0 && i % 2 != 0 && i != 0 && arr[i] != 0)
    {
      res.push_back(arr[i]);
      arr.erase(arr.begin()+i);
    }
  }

  sort(arr.begin(), arr.end());
  reverse(arr.begin(), arr.end());

  foreach(i, arr)
    cout << i << ' ';

  cout << endl;

  foreach(i, res)
    cout << i << ' ';
}

void taskTwo()
{
  vector<int> expOne;
  vector<int> expTwo;
  vector<int> res;
  size_t sizeOne;
  size_t sizeTwo;
  
  cin >> sizeOne >> sizeTwo;

  expOne.resize(sizeOne);
  expTwo.resize(sizeTwo);
  res.resize(max(sizeOne, sizeTwo));

  for (size_t i = 0; i < sizeOne; i++)
    cin >> expOne[i];

  cout << endl;

  for (size_t i = 0; i < sizeTwo; i++)
    cin >> expTwo[i];

  for (size_t i = 0; i != max(sizeOne, sizeTwo); i++)
    res[i] = expOne[i] + expTwo[i];

  cout << "< ";

  for (size_t i = 0; i <= res.size(); i++)
  {

    if (res[i] != 0)
      cout << res[i] << "x^" << res.size() - i;
    
    if (i != res.size() - 1 && res[i] != 0)
      cout << " + ";
  }

  cout << "1 >" << endl;
}  

void fib()
{
  long double prev = 0;
  long double before = 1;
  long double next = 0;

  ofstream file("fib.txt");
  
  for (size_t i = 2; i < 10002; i++)
  {
    next = prev + before;
    before = prev;
    prev = next;
    file.precision(100000);
    file << i - 1 << "\t\t" << next << endl;
  }

  file.close();
}

int main()
{
  // taskOne();
  // cout << "____________" << endl;
  taskTwo();
  // cout << "____________" << endl;
  // fib();
  return 0;
}

